package de.javascripter.connectbungee.config;

import java.io.File;
import java.io.IOException;

import de.javascripter.connectbungee.ConnectBungee;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class DefaultConfig
{
    private Configuration config;
    private ConfigurationProvider yaml;
    private File conf;

    public DefaultConfig()
    {
        conf = new File(ConnectBungee.getInstance().getDataFolder(), "config.yml");
        conf.getParentFile().mkdirs();
        yaml = ConfigurationProvider.getProvider(YamlConfiguration.class);

        try
        {
            if(!conf.exists())
            {
                conf.createNewFile();
                config = yaml.load(conf);
                config.set("proxyname", "exampleproxy1");
                config.set("serverport", 1234);
                config.set("proxylist.exampleproxy2", "127.0.0.1:12345");
                config.set("proxylist.exampleproxy3", "127.0.0.1:1234");
                yaml.save(config, conf);
            }
            else
            {
                config = yaml.load(conf);

                yaml.save(config, conf);
            }
        }
        catch (IOException ex)
        {
            ConnectBungee.getInstance().getProxy().getLogger().warning("Error: Config not loaded/saved.");
        }
    }

    public void reloadConfig()
    {
        try
        {
            if(!conf.exists())
                conf.createNewFile();

            config = yaml.load(conf);

            yaml.save(config, conf);
        }
        catch (IOException ex)
        {
            ConnectBungee.getInstance().getProxy().getLogger().warning("Error: Config not loaded/saved.");
        }
    }

    public void saveConfig()
    {
        try
        {
            yaml.save(config, conf);

            config = yaml.load(conf);
        }
        catch (IOException ex)
        {
            ConnectBungee.getInstance().getProxy().getLogger().warning("Error: Config not loaded/saved.");
        }
    }

    public Configuration getConfig()
    {
        return config;
    }
}
