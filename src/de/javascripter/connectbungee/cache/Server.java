package de.javascripter.connectbungee.cache;

import java.util.Set;

public class Server
{
    private String name;
    private Set<String> players;
    
    public Server(String name, Set<String> players)
    {
        this.name = name;
        this.players = players;
    }
    
    public Set<String> getPlayers()
    {
        return players;
    }
    
    public void setPlayers(Set<String> players)
    {
        this.players = players;
    }
    
    public String getName()
    {
        return name;
    }
}
