package de.javascripter.connectbungee.cache;

public class Proxy
{
    private long timestamp;
    private String name;
    
    public Proxy(String name)
    {
        this.name = name;
        timestamp = System.currentTimeMillis();
    }
    
    public String getName()
    {
        return name;
    }
    
    public void flagUpdated()
    {
        timestamp = System.currentTimeMillis();
    }
    
    public boolean isExpired()
    {
        return System.currentTimeMillis() - timestamp > 5000L;
    }
}
