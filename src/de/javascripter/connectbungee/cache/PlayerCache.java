package de.javascripter.connectbungee.cache;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import de.javascripter.connectbungee.ConnectBungee;

public class PlayerCache
{
    private Map<Proxy, Set<Server>> cache = new ConcurrentHashMap<Proxy, Set<Server>>();

    public PlayerCache()
    {
        for(String s : ConnectBungee.getInstance().getDefaultConfig().getConfig().getSection("proxylist").getKeys())
        {
            addProxy(s);
        }
    }

    public void addProxy(String proxy)
    {
        cache.put(new Proxy(proxy), new HashSet<Server>());
    }

    public Set<Proxy> getProxies()
    {
        return cache.keySet();
    }

    public void reloadProxies()
    {
        for(String s : ConnectBungee.getInstance().getDefaultConfig().getConfig().getSection("proxylist").getKeys())
        {
            cache.clear();

            addProxy(s);
        }
    }

    public Set<String> getPlayers()
    {
        Set<String> totalPlayers = new HashSet<String>();

        for(Proxy proxy : cache.keySet())
        {
            if(proxy.isExpired())
                continue;

            for(Server server : cache.get(proxy))
            {
                totalPlayers.addAll(server.getPlayers());
            }
        }

        return totalPlayers;
    }

    public Set<String> getPlayersOnServer(String server)
    {
        Set<String> players = new HashSet<String>();

        for(Proxy proxy : cache.keySet())
        {
            if(proxy.isExpired())
                continue;

            for(Server srv : cache.get(proxy))
            {
                if(srv.getName().equals(server))
                {
                    players.addAll(srv.getPlayers());
                }
            }
        }

        return players;
    }

    public Set<String> getPlayersOnProxy(Proxy proxy)
    {
        if(proxy.isExpired())
            return null;

        Set<String> players = new HashSet<String>();

        for(Server srv : cache.get(proxy))
        {
            players.addAll(srv.getPlayers());
        }

        return players;
    }

    public Proxy getPlayerProxy(String player)
    {
        for(Proxy proxy : cache.keySet())
        {
            for(Server srv : cache.get(proxy))
            {
                if(srv.getPlayers().contains(player))
                {
                    return proxy;
                }
            }
        }

        return null;
    }

    public void addServer(String server)
    {
        for(Proxy proxy : cache.keySet())
        {
            cache.get(proxy).add(new Server(server, new HashSet<String>()));
        }
    }

    public void removeServer(String server)
    {
        for(Proxy proxy : cache.keySet())
        {
            for(Server s : cache.get(proxy))
            {
                if(s.getName().equals(server))
                {
                    cache.get(proxy).remove(s);
                }
            }
        }
    }

    public boolean containsServer(Proxy proxy, String server)
    {
        for(Server s : cache.get(proxy))
        {
            return(s.getName().equals(server));
        }

        return false;
    }

    public Proxy getProxy(String name)
    {
        for(Proxy p : cache.keySet())
        {
            if(p.getName().equals(name))
            {
                return p;
            }
        }

        return null;
    }
    
    public Server getServer(Proxy p, String srv)
    {
        for(Server s : cache.get(p))
        {
            if(s.getName().equals(srv))
                return s;
        }
        
        return null;
    }
}
