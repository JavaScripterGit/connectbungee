package de.javascripter.connectbungee.events;

import net.md_5.bungee.api.plugin.Event;

public class ProxyMessageEvent extends Event
{
    private String proxy, channel, message;
    
    public ProxyMessageEvent(String proxy, String channel, String message)
    {
        this.proxy = proxy;
        this.channel = channel;
        this.message = message;
    }
    
    public String getProxy()
    {
        return proxy;
    }
    
    public String getChannel()
    {
        return channel;
    }
    
    public String getMessage()
    {
        return message;
    }
}
