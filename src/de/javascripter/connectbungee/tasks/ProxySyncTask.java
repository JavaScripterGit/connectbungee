package de.javascripter.connectbungee.tasks;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import de.javascripter.connectbungee.ConnectBungee;
import de.javascripter.connectbungee.cache.Proxy;
import de.javascripter.connectbungee.messaging.MessageRequest;

public class ProxySyncTask implements Runnable
{
    @Override
    public void run()
    {
        for(Proxy p : ConnectBungee.getInstance().getPlayerCache().getProxies())
        {            
            String data = "";
            for(ServerInfo si : ConnectBungee.getInstance().getProxy().getServers().values())
            {
                data += si.getName() + "-";
                String players = "";
                for(ProxiedPlayer pl : si.getPlayers())
                {
                    players += pl.getName() + ":";
                }
                if(players.length() > 0)
                    players = players.substring(0, players.length() - 1);
                data += players + ">>";
            }

            data = data.substring(0, data.length() - 2);
            
            new MessageRequest(p.getName(), "BungeeConnect", "syncproxy " + ConnectBungee.getInstance().getProxyName() + " " + data);
        }
    }
}
