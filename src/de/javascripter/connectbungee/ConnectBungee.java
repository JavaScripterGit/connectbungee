package de.javascripter.connectbungee;

import java.util.concurrent.TimeUnit;

import de.javascripter.connectbungee.cache.PlayerCache;
import de.javascripter.connectbungee.cache.Proxy;
import de.javascripter.connectbungee.config.DefaultConfig;
import de.javascripter.connectbungee.listeners.MessageListener;
import de.javascripter.connectbungee.listeners.PingListener;
import de.javascripter.connectbungee.messaging.MessageRequest;
import de.javascripter.connectbungee.messaging.MessageServer;
import de.javascripter.connectbungee.tasks.ProxySyncTask;
import net.md_5.bungee.api.plugin.Plugin;

public class ConnectBungee extends Plugin
{
    private static ConnectBungee instance;
    private DefaultConfig config;
    private PlayerCache playerCache;
    private String proxyName;

    @Override
    public void onEnable()
    {
        instance = this;
        config = new DefaultConfig();
        new MessageServer(config.getConfig().getInt("serverport"));
        playerCache = new PlayerCache();
        proxyName = getDefaultConfig().getConfig().getString("proxyname");

        for(Proxy proxy : playerCache.getProxies())
            new MessageRequest(proxy.getName(), "BungeeConnect", "syncproxy " + getProxyName());

        getProxy().getPluginManager().registerListener(this, new MessageListener());
        getProxy().getPluginManager().registerListener(this, new PingListener());

        getProxy().getScheduler().schedule(this, new ProxySyncTask(), 2L, 1L, TimeUnit.SECONDS);
    }

    public static ConnectBungee getInstance()
    {
        return instance;
    }

    public String getProxyName()
    {
        return proxyName;
    }

    public PlayerCache getPlayerCache()
    {
        return playerCache;
    }

    public DefaultConfig getDefaultConfig()
    {
        return config;
    }
}
