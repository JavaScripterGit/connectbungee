package de.javascripter.connectbungee.listeners;

import java.util.HashSet;
import java.util.Set;

import de.javascripter.connectbungee.ConnectBungee;
import de.javascripter.connectbungee.cache.PlayerCache;
import de.javascripter.connectbungee.cache.Proxy;
import de.javascripter.connectbungee.events.ProxyMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class MessageListener implements Listener
{
    @EventHandler
    public void onMessage(ProxyMessageEvent e)
    {
        if(!e.getChannel().equals("BungeeConnect"))
            return;

        String[] data = e.getMessage().split(" ");
        PlayerCache pc;

        switch(data[0])
        {
            case "syncproxy":
                pc = ConnectBungee.getInstance().getPlayerCache();

                String[] payload;

                if(data.length < 3)
                    return;

                if(data[2].contains(">>"))
                    payload = data[2].split(">>");
                else
                    payload = new String[]
                    {data[2]};

                for(String s : payload)
                {
                    String srv = s.split("-")[0];

                    Set<String> players = new HashSet<String>();

                    if(s.contains("-") && s.split("-").length > 1 && s.split("-")[1].contains(":"))
                    {
                        for(String pl : s.split("-")[1].split(":"))
                        {
                            players.add(pl);
                        }
                    }
                    else if(s.contains("-") && s.split("-").length > 1)
                    {
                        players.add(s.split("-")[1]);
                    }
                    else
                    {
                        return; // No servers defined
                    }

                    Proxy p = pc.getProxy(e.getProxy());
                    
                    System.out.println(e.getProxy() + " RECEIVED");

                    p.flagUpdated();
                    
                    System.out.println(p.isExpired());

                    if(pc.containsServer(p, srv))
                    {
                        pc.getServer(p, srv).setPlayers(players);
                    }
                    else
                    {
                        pc.addServer(srv);
                        pc.getServer(p, srv).setPlayers(players);
                    }
                }
                break;
        }
    }
}
