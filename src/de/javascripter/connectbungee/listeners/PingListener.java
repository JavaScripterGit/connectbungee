package de.javascripter.connectbungee.listeners;

import de.javascripter.connectbungee.ConnectBungee;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PingListener implements Listener
{
    @EventHandler
    public void onPing(ProxyPingEvent e)
    {
        ServerPing ping = e.getResponse();
        ping.setPlayers(new ServerPing.Players(ping.getPlayers().getMax(), ConnectBungee.getInstance().getPlayerCache().getPlayers().size(), ping.getPlayers().getSample()));
        e.setResponse(ping);
    }
}
