package de.javascripter.connectbungee.messaging;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

import de.javascripter.connectbungee.ConnectBungee;
import de.javascripter.connectbungee.events.ProxyMessageEvent;

public class MessageServer implements Runnable
{
    private Thread thread;
    private int port;
    private ServerSocket server;

    public MessageServer(int port)
    {
        this.port = port;
        start();
    }

    public void start()
    {
        thread = new Thread(this);
        thread.setName("socketserver-thread");
        thread.start();
        ConnectBungee.getInstance().getProxy().getLogger().info("Socket message server now running on port " + port);
    }

    public void stop()
    {
        if(thread == null)
            return;

        thread.interrupt();
        thread = null;
    }

    public void run()
    {
        try
        {
            server = new ServerSocket(port);
        }
        catch (IOException e)
        {
            ConnectBungee.getInstance().getProxy().getLogger().severe("Server could not be started on port: " + port);
            ConnectBungee.getInstance().getProxy().getLogger().severe("Message server and thread stopped!");
            stop();
        }

        while(true)
        {
            try
            {
                Socket handler = server.accept();
                InputStreamReader ir = new InputStreamReader(handler.getInputStream());
                BufferedReader br = new BufferedReader(ir);

                String ip = handler.getInetAddress().toString().substring(1);

                String proxy = br.readLine();
                String channel = br.readLine();
                String message = br.readLine();

                Set<String> ipAddresses = new HashSet<String>();

                for(String s : ConnectBungee.getInstance().getDefaultConfig().getConfig().getSection("proxylist").getKeys())
                {
                    ipAddresses.add(ConnectBungee.getInstance().getDefaultConfig().getConfig().getString("proxylist." + s).split(":")[0]);
                }

                if(ipAddresses.contains(ip))
                {
                    ConnectBungee.getInstance().getProxy().getPluginManager().callEvent(new ProxyMessageEvent(proxy, channel, message));
                }

                handler.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
