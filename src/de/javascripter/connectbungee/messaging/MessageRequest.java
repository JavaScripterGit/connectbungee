package de.javascripter.connectbungee.messaging;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import de.javascripter.connectbungee.ConnectBungee;

public class MessageRequest implements Runnable
{
    private Thread thread;
    private Socket client;
    private String proxy, channel, message;

    public MessageRequest(String proxy, String channel, String message)
    {
        this.proxy = proxy;
        this.channel = channel;
        this.message = message;
        
        String[] ipPort = ConnectBungee.getInstance().getDefaultConfig().getConfig().getString("proxylist." + proxy).split(":");
        
        InetAddress ip = null;
        
        try
        {
            ip = InetAddress.getByName(ipPort[0]);
        }
        catch (UnknownHostException e1)
        {
            e1.printStackTrace();
        }
        
        int port = Integer.parseInt(ipPort[1]);
        
        try
        {
            client = new Socket(ip, port);
            start();
        }
        catch (IOException e)
        {
            ConnectBungee.getInstance().getProxy().getLogger().warning("Error: Could not connect to proxy: " + proxy + " - Retrying...");
        }
    }

    public void start()
    {
        thread = new Thread(this);
        thread.start();
    }

    public void stop()
    {
        if(thread == null)
            return;

        thread.interrupt();
        thread = null;
    }

    public void run()
    {
        PrintStream out = null;
        try
        {
            out = new PrintStream(client.getOutputStream());

            out.println(proxy);
            out.println(channel);
            out.println(message.replace("\n", ""));
            client.close();
            stop();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}