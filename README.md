ConnectBungee

---------

Decentralized proxy sync system that utalizes sockets to send data between BungeeCord proxies.

------------------------------

Setup Information
-----------
1. Run the plugin once to generate the config
2. Change the name of the proxies to your liking (do not use the same name on 2 proxies)
3. Change the port to any open port that you would like ConnectBungee to use
4. Put all IP's and ports (defined in the ConnectBungee config) of all proxies (including the one you are editing) in the proxy list
